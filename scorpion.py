from random import randint
from math import radians
from math import sin
from math import sqrt
from math import pow
import matplotlib.pyplot as plt
import numpy as np

class ScorpionGame:

	def __init__(self, options):
		# Initialisation des variables
		self.options = options
		self.iteration = 1
		self.population = ''
		self.distances = {}
		self.energie = {}
		self.fitness = {}
		self.results = {
			'variance':{
				'x':[],
				'y':{
					'moyenne':[],
					'variance':[],
					'min':[],
					'max':[]
				}	
			},
			'distance':{
				'x':[],
				'y':{
					'moyenne':[],
					'target':[],
					'min':[],
					'max':[]
				}	
			},
			'energie':{
				'x':[],
				'y':{
					'moyenne':[],
					'min':[],
					'max':[]
				}	
			}
		}
		# Début du script
		self.start()
	
	# Dessiner les graphes
	def draw_results(self):
		plt.figure(1)
		# Distances
		plt.subplot(221)
		plt.plot(self.results['distance']['x'], self.results['distance']['y']['moyenne'],'y')
		plt.plot(self.results['distance']['x'], self.results['distance']['y']['target'],'r')
		plt.plot(self.results['distance']['x'], self.results['distance']['y']['min'],'g')
		plt.plot(self.results['distance']['x'], self.results['distance']['y']['max'],'b')
		plt.title('Distances')
		plt.grid(True)

		# Energies
		plt.subplot(222)
		plt.plot(self.results['energie']['x'], self.results['energie']['y']['moyenne'],'y')
		plt.plot(self.results['energie']['x'], self.results['energie']['y']['min'],'g')
		plt.plot(self.results['energie']['x'], self.results['energie']['y']['max'],'b')
		plt.title('Energies')
		plt.grid(True)

		# Variance
		plt.subplot(223)
		plt.plot(self.results['distance']['x'], self.results['variance']['y']['variance'],'y')
		plt.title('Variance')
		plt.grid(True)

		# Scores
		plt.subplot(224)
		plt.plot(self.results['distance']['x'], self.results['variance']['y']['moyenne'],'y')
		plt.plot(self.results['distance']['x'], self.results['variance']['y']['min'],'g')
		plt.plot(self.results['distance']['x'], self.results['variance']['y']['max'],'b')
		plt.title('Scores')
		plt.grid(True)

		plt.show()

	# Début du script
	def start(self):
		#Vérification du nombre de scorpion à générer
		if(self.options['populationNumber']>0):
			#Générer les scorpions
			self.populate_scorpion(self.options['populationNumber'])
			#Calculer la distance et l'énergie de chaque scroption
			self.calculate_distance_energie()

			run = True
			#Tant que l'iteration max n'est pas atteint et que la moyenne des distance n'est pas à
			#plus ou moins pourcDistanceAcceptee% de la cible, on regénère des scoprions
			while(self.iteration < self.options['maxIteration'] and run):
				#Lancement des nouvelles généréations
				self.init_gene()
				#Calculer la distance et l'énergie de chaque scroption
				self.calculate_distance_energie()
				#Calcul du poids de chaque scorpion
				self.calculate_total_fitness()
				#Selection des corpions
				self.select_scoprions()
				#Vérifier si nous sommes à plus de 5 générations
				if(self.iteration > 5):
					#Vérifier que la a moyenne des distance n'est pas à
					#plus ou moins pourcDistanceAcceptee% de la cible
					moyenne = (self.results['distance']['y']['moyenne'][self.iteration-2]
						+self.results['distance']['y']['moyenne'][self.iteration-3]
						+self.results['distance']['y']['moyenne'][self.iteration-4]
						+self.results['distance']['y']['moyenne'][self.iteration-5]
						+self.results['distance']['y']['moyenne'][self.iteration-6])/5
					if(moyenne>int(self.options['distanceCible']*(1-self.options['pourcDistanceAcceptee']/100)) and moyenne<int(self.options['distanceCible']*(1+self.options['pourcDistanceAcceptee']/100))):
						run=False
			#Dessiner les graphes
			self.draw_results()

	#Lancement des nouvelles généréations
	def init_gene(self):
		self.iteration += 1
		number = 1
		couples = self.sus()
		for couple in couples:
			self.generate_children(couple, number)
			number += 2

	#Générer les enfants graces aux couples
	def generate_children(self, couple, number):
		if(randint(0, 1) == 1):
			self.gen_parser(couple, number, 5)
		else:
			self.gen_parser(couple, number, randint(1, 9))

	#Découper les gènes en deux groupes pour les affecter aux enfants
	#Gérer les mutations
	def gen_parser(self, couple, number, stop):
		scorpionOne = self.get_scorpion(couple[0]).split(';')
		scorpionTwo = self.get_scorpion(couple[1]).split(';')
		childrenOne = []
		childrenTwo = []
		mutationOne = -1
		mutationTwo = -1
		canMutateOne = randint(0,100)
		canMutateTwo = randint(0,100)
		if(canMutateOne<self.options['pourcMutation']):
			mutationOne = randint(0,10)
		if(canMutateTwo<self.options['pourcMutation']):
			mutationTwo = randint(0,10)
		for index in range(len(scorpionOne)):
			if(mutationOne == index):
				childrenOne.insert(index, self.get_rand_value(index))
			else:
				if(index < stop):
					childrenOne.insert(index, scorpionOne[index])
				else:
					childrenOne.insert(index, scorpionTwo[index])
			if(mutationTwo == index):
				childrenTwo.insert(index, self.get_rand_value(index))
			else:
				if(index < stop):
					childrenTwo.insert(index, scorpionTwo[index])
				else:
					childrenTwo.insert(index, scorpionOne[index])

		identifiantOne = str(self.iteration)+'-'+str(number)
		identifiantTwo = str(self.iteration)+'-'+str(number+1)
		self.population += '|'+'{'+str(identifiantOne)+'}'+';'.join(childrenOne)
		self.population += '|'+'{'+str(identifiantTwo)+'}'+';'.join(childrenTwo)
		self.distances[identifiantOne] = ''
		self.distances[identifiantTwo] = ''
		self.energie[identifiantOne] = ''
		self.energie[identifiantTwo] = ''

	#Selection des corpions
	def select_scoprions(self):
		sortedArray = sorted(self.fitness, key=self.fitness.__getitem__, reverse=True)
		population = ''
		distances = {}
		energie = {}
		maxDistance = -1
		minDistance = -1
		moyenneDistance = 0
		maxEnergie = -1
		minEnergie = -1
		moyenneEnergie = 0
		viarances = []
		for key in sortedArray:
			if(len(distances)>=self.options['populationNumber']):
				break
			viarances.append(self.fitness[key])
			distances[key]=self.distances[key]
			energie[key]=self.energie[key]
			#MAX
			if(maxDistance < distances[key]):
				maxDistance = distances[key]
			if(maxEnergie < energie[key]):
				maxEnergie = energie[key]
			#MIN
			if(minDistance > distances[key] or minDistance == -1):
				minDistance = distances[key]
			if(minEnergie > energie[key] or minEnergie == -1):
				minEnergie = energie[key]
			#SOMME
			moyenneDistance += distances[key]
			moyenneEnergie += energie[key]

			if(population != ''):
				population += '|'
			population += '{'+key+'}'+self.get_scorpion(key)
		self.distances = distances
		self.population = population
		self.energie = energie
		moyenneDistance = moyenneDistance/self.options['populationNumber']
		moyenneEnergie = moyenneEnergie/self.options['populationNumber']
		key = self.iteration-1
		self.results['distance']['x'].insert(key,int(self.iteration))
		self.results['distance']['y']['target'].insert(key,self.options['distanceCible'])
		self.results['distance']['y']['moyenne'].insert(key,moyenneDistance)
		self.results['distance']['y']['max'].insert(key,maxDistance)
		self.results['distance']['y']['min'].insert(key,minDistance)
		self.results['energie']['x'].insert(key,self.iteration)
		self.results['energie']['y']['moyenne'].insert(key,moyenneEnergie)
		self.results['energie']['y']['max'].insert(key,maxEnergie)
		self.results['energie']['y']['min'].insert(key,minEnergie)
		self.results['variance']['x'].insert(key,self.iteration)
		self.results['variance']['y']['moyenne'].insert(key,np.mean(viarances))
		self.results['variance']['y']['variance'].insert(key,np.var(viarances))
		self.results['variance']['y']['max'].insert(key,np.max(viarances))
		self.results['variance']['y']['min'].insert(key,np.min(viarances))

	#Récupérer une nouvelle valeur lors de la mutation
	def get_rand_value(self, index):
	    return str({
	        0: self.get_angle(), #angle
			1: self.get_longueur_bras(), #longueurBras
			2: self.get_hauteur_largeur_base_bras(self.get_longueur_bras()), #largeurBaseBras
			3: self.get_hauteur_largeur_base_bras(self.get_longueur_bras()), #hauteurBaseBras
			4: self.options['materiauxScorpionSelected'], #materiau
			5: self.get_longueur_corde(self.get_longueur_bras()), #longueurCorde
			6: self.get_longueur_fleche(self.get_longueur_bras()), #longueurFleche
			7: self.get_hauteur_largeur_base_fleche(self.get_longueur_fleche(self.get_longueur_bras())), #largeurBaseFleche
			8: self.get_hauteur_largeur_base_fleche(self.get_longueur_fleche(self.get_longueur_bras())), #hauteurBaseFleche
			9: self.options['materiauxFlecheSelected'], #materieauFleche
	    }[index])

	#Générer les scorpions
	def populate_scorpion(self, count):
		while(count > 0):
			self.create_scorpion(count)
			count -= 1

	#Créer un scorpion et lui assigner un identifiant de type NGénération-NScorpion
	def create_scorpion(self, count):
		angle = self.get_angle()
		longueurBras = self.get_longueur_bras()
		largeurBaseBras = self.get_hauteur_largeur_base_bras(longueurBras)
		hauteurBaseBras = self.get_hauteur_largeur_base_bras(longueurBras)
		materiau = self.options['materiauxScorpionSelected']
		longueurCorde =self.get_longueur_corde(longueurBras)

		longueurFleche = self.get_longueur_fleche(longueurBras)
		largeurBaseFleche = self.get_hauteur_largeur_base_fleche(longueurFleche)
		hauteurBaseFleche = self.get_hauteur_largeur_base_fleche(longueurFleche)
		materieauFleche = self.options['materiauxFlecheSelected']

		identifiant = str(self.iteration)+'-'+str(self.options['populationNumber']+1-count)

		scorpion = self.create_scorpion_to_string([
				identifiant,
				angle,
				longueurBras,
				largeurBaseBras,
				hauteurBaseBras,
				materiau,
				longueurCorde,
				longueurFleche,
				largeurBaseFleche,
				hauteurBaseFleche,
				materieauFleche
			])

		if(self.population != ''):
			self.population += '|'
		self.population += scorpion
		self.distances[identifiant] = ''
		self.energie[identifiant] = ''

	#Parser le scorpion en chaine de charactères
	def create_scorpion_to_string(self, params):
		scorpion = '{'+params[0]+'}' #identifiant
		scorpion += str(params[1])+";" #angle
		scorpion += str(params[2])+";" #longueurBras
		scorpion += str(params[3])+";" #largeurBaseBras
		scorpion += str(params[4])+";" #hauteurBaseBras
		scorpion += str(params[5])+";" #materieau
		scorpion += str(params[6])+";" #longueurCorde
		scorpion += str(params[7])+";" #longueurFleche
		scorpion += str(params[8])+";" #largeurBaseFleche
		scorpion += str(params[9])+";" #hauteurBaseFleche
		scorpion += str(params[10]) #materieauFleche
		return scorpion

	#Vérifier si le scorpion n'atteint pas ses limites et se casse
	def is_not_broken(self, scorpion):
		scorpion = scorpion.split(';')
		longueurBras = float(scorpion[1])
		largeurBaseBras = float(scorpion[2])
		hauteurBaseBras = float(scorpion[3])
		longueurCorde = float(scorpion[5])
		result = False
		if(longueurBras>longueurCorde):
			longueurVide = self.get_longueur_vide(longueurBras, longueurCorde)
			if(longueurVide > float(scorpion[6])):
				quadratique = (largeurBaseBras*pow(hauteurBaseBras,3))/12
				young = float(self.options['materiaux'][int(scorpion[4])]['young'])
				coef = float(self.options['materiaux'][int(scorpion[4])]['coef'])
				ressort = self.get_ressort(young, coef)
				longueurDeplacement = self.get_longueur_deplacement(float(scorpion[6]), float(scorpion[1]), float(scorpion[5]))
				forceF = ressort*longueurDeplacement
				if(longueurDeplacement<(forceF*pow(longueurBras,3))/(48*young*quadratique)):
					result=True
		return result

	#Calcul du poids de chaque scorpion
	def calculate_total_fitness(self):
		self.fitness = {}
		somme = 0
		maxEnergie = self.get_max_value(self.energie)
		for key, value in self.distances.items():
			distancePoints = 0
			addPoints = 0
			if(value>0):
				if(value<50):
					addPoints += int(80*(value*2/100))+20
				else:
					addPoints += 100
			if(self.energie[key]>0):
				if(self.energie[key]<20):
					addPoints += int(80*(self.energie[key]*5/100))+20
				else:
					addPoints += 100
			if(value < 2*self.options['distanceCible']):
				if(value > self.options['distanceCible']):
					distancePoints = int((abs(self.options['distanceCible']-(value-self.options['distanceCible'])))/self.options['distanceCible']*400)
				elif(value == self.options['distanceCible']):
					distancePoints = 400
				else:
					distancePoints = int(value/self.options['distanceCible']*400)
			if(self.energie[maxEnergie] != float(0)):
				result = distancePoints+int(self.energie[key]/self.energie[maxEnergie]*100)+1
			else:
				result = distancePoints+1
			result = result+addPoints
			self.fitness[key] = result
			somme += result
		return somme

	#Récupérer la valeur max d'un tableau
	def get_max_value(self, array):
		maxValue = 0
		keyValue = ''
		for key, value in array.items():
			if(maxValue<value or keyValue == ''):
				maxValue = value
				keyValue = key
		return keyValue

	#Calculer la distance et l'énergie de chaque scroption
	def calculate_distance_energie(self):
		for key, value in self.distances.items():
			scorpion = self.get_scorpion(key)
			if(self.is_not_broken(scorpion)):
				self.distances[key] = self.get_distance(scorpion)
				self.energie[key] = self.get_energie(scorpion)
			else:
				self.distances[key] = float(0)
				self.energie[key] = float(0)

	#Calculer l'énergie d'un scorpion
	def get_energie(self, scorpion):
		scorpion = scorpion.split(';')
		massevFleche = float(self.options['materiaux'][int(scorpion[9])]['massev'])
		masseFleche = self.get_masse_fleche(massevFleche,float(scorpion[6]),float(scorpion[7]),float(scorpion[8]))
		young = float(self.options['materiaux'][int(scorpion[4])]['young'])
		coef = float(self.options['materiaux'][int(scorpion[4])]['coef'])
		ressort = self.get_ressort(young, coef)
		longueurDeplacement = self.get_longueur_deplacement(float(scorpion[6]), float(scorpion[1]), float(scorpion[5]))
		velocite = self.get_velocite(ressort, longueurDeplacement, masseFleche)
		energie = 1/2*masseFleche*pow(velocite,2)
		return energie

	#Calculer la distance d'un scorpion
	def get_distance(self, scorpion):
		scorpion = scorpion.split(';')
		young = float(self.options['materiaux'][int(scorpion[4])]['young'])
		coef = float(self.options['materiaux'][int(scorpion[4])]['coef'])
		ressort = self.get_ressort(young, coef)
		massevFleche = float(self.options['materiaux'][int(scorpion[9])]['massev'])
		masseFleche = self.get_masse_fleche(massevFleche,float(scorpion[6]),float(scorpion[7]),float(scorpion[8]))
		longueurDeplacement = self.get_longueur_deplacement(float(scorpion[6]), float(scorpion[1]), float(scorpion[5]))
		velocite = self.get_velocite(ressort, longueurDeplacement, masseFleche)
		distance = (pow(velocite,2)/self.options['environnement'])*sin(radians(float(scorpion[0])*2))
		return distance

	#Calculer la velocité
	def get_velocite(self, ressort, longueurDeplacement, masseFleche):
		return sqrt((ressort*pow(longueurDeplacement,2))/masseFleche)

	#Calculer la force du ressort
	def get_ressort(self, young, coef):
		return (1/3)*(young/(1-2*coef))

	#Calculer la longueur à vide
	def get_longueur_vide(self, longueurBras, longueurCorde):
		calcul = pow(longueurBras,2)-(1/4)*pow(longueurCorde,2)
		if(calcul <= 0):
			distanceVide = 0
		else:
			distanceVide = 1/2*sqrt(calcul)
		return distanceVide

	#Calculer la longueur de déplacement
	def get_longueur_deplacement(self, longueurFleche, longueurBras, longueurCorde):
		return longueurFleche-self.get_longueur_vide(longueurBras, longueurCorde)

	#Calculer la masse de la fleche
	def get_masse_fleche(self, massev, longueurFleche, largeurBaseFleche, hauteurBaseFleche):
		return massev*longueurFleche*largeurBaseFleche*hauteurBaseFleche

	#Récupérer un scorpion via son identifiant
	def get_scorpion(self, identifiant):
		scorpion = self.population.split('{'+identifiant+'}',1)[1]
		scorpion = scorpion.split('|',1)[0]
		return scorpion

	#Fonction SUS
	def sus(self):
		totalFitnessPop = self.calculate_total_fitness()
		numberOffspring = self.options['populationNumber']
		distancePointers = totalFitnessPop/numberOffspring
		start = randint(0,int(distancePointers*1000))/1000
		fitnessValueList = list(self.fitness.values())
		fitnessKeyList = list(self.fitness.keys())
		
		keep = []
		count = 0
		oldScorpion = ''
		while True:
			point = start + count*distancePointers
			count += 1
			scorpion = self.find_next_scorpion_with_point(fitnessKeyList, fitnessValueList, point)
			if(oldScorpion != '' and scorpion != oldScorpion and scorpion != ''):
				keep.append((oldScorpion,scorpion))
				oldScorpion = ''
			else:
				oldScorpion = scorpion
			if(len(keep)>=self.options['populationNumber']/2):
				break
		return keep

	#Réupérer le prochain scorpion pour former des couple
	def find_next_scorpion_with_point(self, fitnessKeyList, fitnessValueList, point):
		somme = 0
		number = -1
		scorpion = ''
		while somme < point:
			number += 1
			somme += fitnessValueList[number%self.options['populationNumber']]
		scorpion = fitnessKeyList[number%self.options['populationNumber']]
		return scorpion

	#Créer l'angle du scorpion
	def get_angle(self):
		return randint(1,89)

	#Créer la longueur du bras du scorpion
	def get_longueur_bras(self):
		return randint(5,self.options['longueurBras']*10)/10

	#Créer la hauteur/largeur de la base du bras 
	def get_hauteur_largeur_base_bras(self, longueurBras):
		return randint(1,int(longueurBras*10000*self.options['pourcBrasBaseBras']/100))/10000

	#Créer la longueur de la fleche
	def get_longueur_fleche(self, longueurBras):
		return randint(int(longueurBras*100*(self.options['pourcMinBrasLongueurFleche'])/100),
			int(longueurBras*100*(self.options['pourcMaxBrasLongueurFleche'])/100))/100

	#Créer la hauteur/largeur de la base de la flèche
	def get_hauteur_largeur_base_fleche(self, longueurFleche):
		return randint(1,int(longueurFleche*10000*self.options['pourcFlecheBaseFleche']/100))/10000

	#Créer la longueur de la corde du scorpion
	def get_longueur_corde(self, longueurBras):
		return randint(int(longueurBras*10000*(self.options['pourcBrasLongueurCorde'])/100),longueurBras*10000)/10000

environnement = {
	"Terre":9.81,
	"Lune":1.62,
	"Jupiter":24.80
}

options = {}
options['populationNumber'] = 700
#longueur max du bras en metre
options['longueurBras'] = 10
#largeur/hauteur max de la base du bras en pourcentage de la taille du bras
options['pourcBrasBaseBras'] = 30
#longueur max de la fleche en pourcentage de la taille du bras
options['pourcMaxBrasLongueurFleche'] = 110
#longueur minimale de la flèche en pourcentage de la longueur du bras
options['pourcMinBrasLongueurFleche'] = 85
#largeur/hauteur max de la base de la fleche en pourcentage de la taille de la fleche
options['pourcFlecheBaseFleche'] = 5
#longueur minimal de la corde
options['pourcBrasLongueurCorde'] = 80
#liste des matériaux et leur données
options['materiaux'] =[
    {"name":"Acier", "massev":7850, "young":210, "coef": 0.33},
    {"name":"Aluminium", "massev":2700, "young":65, "coef": 0.33},
    {"name":"Argent", "massev":10500, "young":78, "coef": 0.37},
    {"name":"Beton", "massev":2400, "young":30, "coef": 0.20},
    {"name":"Bois", "massev":800, "young":11, "coef": 0.4},
    {"name":"Fonte", "massev":7100, "young":105, "coef": 0.29},
    {"name":"Zinc", "massev":7150, "young":78, "coef": 0.21},
    {"name":"Verre", "massev":4500, "young":60, "coef": 0.25},
    {"name":"Caoutchouc", "massev":1800, "young":0.2, "coef": 0.2},
    {"name":"Marbre", "massev":4000, "young":7, "coef": 0.3},
    {"name":"Fonte", "massev":7100, "young":105, "coef": 0.29},
    {"name":"Fonte", "massev":7100, "young":105, "coef": 0.29}
]
#Sélection du matériau utilisé pour le scorpion
options['materiauxScorpionSelected'] = 0
#Sélection du matériau utilisé pour la fleche
options['materiauxFlecheSelected'] = 0
#Choix de l'environnement
options['environnement'] = environnement['Terre']
#Distance de la cible
options['distanceCible'] = 300
#choisir la plus ou moins le pourcentage de la distance autorisée pour terminer le scorpion
options['pourcDistanceAcceptee'] = 2
#Taux de mutation
options['pourcMutation'] = 30
#Teration maximal avant interuption
options['maxIteration'] = 60

game = ScorpionGame(options)
